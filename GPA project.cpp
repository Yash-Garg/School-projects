#include<iostream>
#include<string.h>
#include<conio.h>
#include<windows.h>
using namespace std;
string arr[] = {"Physics", "Chemistry" , "Maths"};
float marks_input(float (&um)[3],float (&sm)[3],int n) {
		
	for(int i=0;i<3;i++){
		b:
		cout<<"Enter total UT marks scored in "<<arr[i]<<" ";
		cin>>um[i];
			if(um[i]>(25*n)){
			cout<<"Enter a value less than or equal to "<<(25*n)<<endl;
			cout<<"Press any key to retry..\n";
			getch();
			system("cls");
			goto b;
		}
		a:
		cout<<"Enter your Final semester marks scored in "<<arr[i]<<" ";
		
		cin>>sm[i];
		if(sm[i]>100){
			cout<<"Enter a valid value \n";
			cout<<"Press any key to retry..\n";
			getch();
			system("cls");
			goto a;
		}
	}
}

void fail (float perc[],int &f){
	for(int i=0;i<3;i++){
		if (perc[i]<33.0){
			f++;
		}		
	}
		if(f==3){
		system("color C0");
		cout<<"Unfortunately you failed in all 3 subjects :/";
	}else if(f==2) {
		system("color E0");
		cout<<"Unfortunately you failed in all 2 subjects :/";
	}else if(f==1) {
		system("color F0");
		cout<<"Unfortunately you failed in 1 subject :/";
	}else {
		system("color A0");
		cout<<"Congratulations you passed in all 3 subjects :D";
	}
}

float percent(float um[],float sm[], float (&perc)[3] ,int n){
	
	for(int i=0;i<3;i++){
	perc[i]=((um[i]+sm[i])/((25*n)+100))*100;
	}

}
float gpa_calc(float perc[], float (&gpa)[3], float &cgpa){
	for(int i=0;i<3;i++){
		gpa[i]=perc[i]/20;
		cgpa+=gpa[i];
	}
	cgpa/=3;
}

int main() {

	// INITIALISATION
	cout<<"This is the GPA and CGPA Calculator \n";
	system("color b2");
	cout<<"Press any key to continue to enter your marks";
	getch();
	system("cls");	

	
	int n, f=0; //Integer Variables Declared , n= # of UT , f = no of subjects failed
	float um[3],sm[3],perc[3],gpa[3], cgpa=0; // Float Variables Declared
	// um = UT total marks , Sm = SA total marks, Perc = Subject Percentage, gpa= Subject GPA 
	// cgpa = total cgpa
	
	//NUMBER OF UT
	cout<<"Enter the Number of UT(s)you took \n";
	cin>>n;
	system("cls");
	
	marks_input(um,sm,n); // Entering the marks...
	percent(um,sm,perc,n); // Percentage Calculator Function Called
	gpa_calc(perc,gpa,cgpa); // GPA and CGPA Calculator Function Called
	
	
	system("cls");
	cout<<"Compiling your results...\n";
	Sleep(2000);
	system("cls");
	
	fail(perc,f); // Calculating # of subjects failed in and displaying it

	cout<<"\n\nPress any key to view full result.. \n";
	getch();
	system("cls");
	
	
	// RESULTS
	cout<<"RESULTS \n................................\n";
	cout<<"PERCENTAGE\n";
	for(int i=0;i<3;i++){
		cout<<arr[i]<<":"<<perc[i]<<"%\n";
	}
	cout<<".......................\n";
	cout<<"GPA \n";
	for(int i=0;i<3;i++){
		cout<<arr[i]<<":"<<gpa[i]<<"\n";
	}
	cout<<".......................\n";
	cout<<"\n\nOverall CGPA is "<<cgpa;

cout<<"Press any key to exit..";
getch();
}
